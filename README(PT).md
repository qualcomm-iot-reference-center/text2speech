# Text2Speech

Aplicação desenvolvida para Dragonboard 410c usando Python3 que transforma os textos de uma imagem em fala.

## Introdução

A aplicação captura a imagem da câmera, detecta e reconhece o texto presente na foto e o reproduz no dispositivo de áudio padrão, podendo traduzi-lo também caso haja conexão com a internet. 

O código da aplicação em Python está disponível no final do tutorial, através do repositório no GitLab.
O processo de detecção da área de texto é feito pelo OpenCV 3.4, que recorta a área de interesse da imagem. A região recortada passa então por um pré-processamento (através do OpenCV também) a fim de facilitar o reconhecimento do texto pelo Tesseract, que retornará uma string de texto.  

O código foi desenvolvido usando o pyttsx3 para transformação do texto em fala. 
Caso haja conexão com a internet, um segundo código pode ser usado, fazendo uso da API do Google gTTS, que grava um áudio com a string em um arquivo mp3. O código também possibilita a tradução do texto através da API de tradução do Google, googletrans.
 

## Equipamento

Os equipamentos necessários para a montagem da aplicação são: 

* DragonBoard 410c; 
* Fonte 12V
* Cartão micro SD (mínimo 8GB, recomendado 16GB)
* Câmera USB, de preferência móvel para facilitar o enquadramento do texto (no projeto foi utilizada uma Logitech C920); 
* Monitor com entrada HDMI; 
* Teclado USB;
* Mouse USB. 



## Instalação
------------------------------------------------------------------------------------

Siga o tutorial deste [link](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md) para ativar a área de Swap no cartão SD.

Para a instalação do OpenCV, siga este [tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/opencv_instalation.md)
------------------------------------------------------------------------

- Tesseract: Para instalar o Tesseract e os bindings para Python execute os seguintes comandos no terminal: 

```sh
sudo apt-get update

sudo apt-get install tesseract-ocr libtesseract-dev espeak -y

sudo apt-get install python3-tk

tesseract -v #verifica a versão do Tesseract 

sudo apt-get install python-pip python3-pip -y

python3 -m pip install pytesseract #instala os bindings

bash docs/install_traineddata_tesseract.sh # bibliotecas para reconhecimento dos caracteres de cada linguagem
```
 
- Demais módulos: Execute os comandos no terminal para instalar os demais módulos (isto pode demorar algumas horas): 

```sh
sudo apt-get install libatlas-base-dev gfortran v4l-utils -y

sudo apt-get install expect 

sudo apt-get install mpg321 -y

python3 -m pip install Pillow numpy scipy gTTS pyttsx3

sudo git clone https://github.com/BoseCorp/py-googletrans.git #o repositório oficial contém um erro 

cd ./py-googletrans 

sudo python3 setup.py install 
```

## Executando o Código 

 

Existem duas versões do código, uma offline que faz o uso do pyttsx3 e outra online usando o gTTS. Para copiar o repositório: 
```sh
git clone https://github.com/QualcommIoTReferenceCenter/Text2Speech.git
cd Text2Speech
```

Antes de iniciar a aplicação, caso for utilizar o recurso de saída de aúdio, estabeleça conexão bluetooth com o dispositivo 
que será utilizado, para isto execute:

```sh
bash docs/bluetooth.sh 
```

Caso seja deseje fazer a conexão manualmente do bluetooth na Dragonboard, siga o seguinte passo a passo:

```sh
sudo bluetoothctl

scan on # Anote o [MAC ADDRESS] do dispositivo que deseja conectar

trust [MAC ADDRESS]

pair [MAC ADDRESS]

connect [MAC ADDRESS]
```

Abra a aplicação executando o comando:

```sh
python3 App.py 
```

**Possível erro durante a execução**

Pode ser que ocorra algum erro devido ao programa não conseguir criar o arquivo .png para o Tesseract fazer o reconhecimento. Caso isso ocorra, mude as permissões das pastas da seguinte forma: 

```sh
cd ..

sudo chmod 777 Text2Speech 

cd Text2Speech 

sudo chmod 777 libs 
```

Outro erro que pode ocorrer é quanto a definição do display que está utilizando, caso ocorra este problema pode ser solucionado através do seguinte comando:

```sh
DISPLAY=:0.0
```

**Exemplos do funcionamento**

*Exemplo 1* - Funcionamento do App

Na tela da interface há as duas opções de funcionamento do código: online e off-line. O modo off-line realiza a leitura em português e inglês, de acordo com a opção escolhida, e o texto é reproduzido em formato de áudio. 
No modo online, é utilizada a API do Google (gTTS), que realiza a tradução e reprodução do texto e as linguagens podem ser escolhidas.

 <div align="center">
    <figure>
        <img src="/Tutorial/App_read_trans.png">
        <figcaption>Interface</figcaption>
    </figure>
</div>

Para realizar a leitura, aponte a webcam para o texto, escolha uma das opções na tela e aperte o botão leitura. Após o processamento, o texto que está no vídeo será escrito e reproduzido pela interface.

 <div align="center">
    <figure>
        <img src="/Tutorial/a_02.png">
        <figcaption>Modo Leitura</figcaption>
    </figure>
</div>

Para realizar a tradução, escolha as opções de linguagens e aperte o botão traduzir. Após o processamento, a API realizará a tradução do texto, o texto será exibido e reproduzido em áudio na interface.

 <div align="center">
    <figure>
        <img src="/Tutorial/App_trans.png">
        <figcaption>Modo Tradução</figcaption>
    </figure>
</div>

*Exemplo 2* - A seguir estão apresentadas algumas imagens de como a aplicação detecta o texto:

 <div align="center">
    <figure>
        <img src="/Tutorial/ok_01.PNG">
        <figcaption>Detecção de palavras simples (impressão em papel)</figcaption>
    </figure>
</div>

 <div align="center">
    <figure>
        <img src="/Tutorial/ok_02.PNG">
        <figcaption>Detecção de frase completa (impressão em papel)</figcaption>
    </figure>
</div>

 <div align="center">
    <figure>
        <img src="/Tutorial/ok_03.PNG">
        <figcaption>Detecção de texto completo (tela de e-reader)</figcaption>
    </figure>
</div>

 <div align="center">
    <figure>
        <img src="/Tutorial/App_read_text.png">
        <figcaption>Detecção de texto completo com caracteres especiais (impressão em papel)</figcaption>
    </figure>
</div>

Após realizar a leitura ou a tradução, é necessário limpar as configurações com um duplo-clique em qualquer parte da interface, para que seja possível a exibição de um novo texto. 

 **Limitações**

 A detecção de texto depende muito da qualidade da imagem fotografada. Para melhor funcionamento da aplicação, recomendamos utilizar uma câmera USB móvel para facilitar a movimentação e o enquadramento, atentar-se à inclinação para que o texto fique legível, e evitar reflexos que comprometam a superfície a ser fotografada (em papel os reflexos não apresentam muito problema, mas realizando testes com texto em telas de celulares, o fator influencia bastante no resultado).


Por fim, como o recurso de tradução do texto utiliza a API do Google tradutor, pequenos erros podem ocorrem, principalmente em frases e palavras isoladas de contexto.
