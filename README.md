# Text to Speech

Application developed for Dragonboard 410c using Python3 that transforms texts from an image into speech.

## Introduction

The application captures the camera image, detects and recognizes the text in the photo and plays it on the standard audio device, and can also translate it if there is an internet connection. 

The application code in Python is available at the end of the tutorial, through the repository in GitLab.
The process of detecting the text area is done by OpenCV 3.4, which cuts the area of ​​interest of the image. The trimmed region then goes through a pre-processing (through OpenCV as well) in order to facilitate the recognition of the text by Tesseract, which will return a text string.

The code was developed using pyttsx3 for the transformation of text into speech.
If there is a connection to the internet, a second code can be used by making use of the Google gTTS API, which records an audio with the string in an mp3 file. The code also allows the translation of the text through the Google translation API, googletrans.
 

## Equipment

The necessary equipment for the assembly of the application are:

* DragonBoard 410c; 
* 12V Wall adapter;
* SD card (16GB);
* USB webcam (a Logitech C920 was used); 
* HDMI Monitor; 
* USB Keyboard;
* USB Mouse. 



## Instalation
------------------------------------------------------------------------------------

Follow the tutorial in this [link](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(en).md) to use the SD card for Swap memory.

For the OpenCV installation, follow this [tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/opencv_installation(en).md)
------------------------------------------------------------------------

- Tesseract: To install Tesseract and bindings for Python run the following commands on the terminal: 

```sh
sudo apt-get update

sudo apt-get install tesseract-ocr libtesseract-dev espeak -y

sudo apt-get install python3-tk

tesseract -v #check the Tesseract version 

sudo apt-get install python-pip python3-pip -y

python3 -m pip install pytesseract #install bindings

bash docs/install_traineddata_tesseract.sh # libraries for recognizing the characters of each language
```
 
- Run the commands on the terminal to install the other modules (this may take a few hours): 

```sh
sudo apt-get install libatlas-base-dev gfortran v4l-utils -y

sudo apt-get install expect 

sudo apt-get install mpg321 -y

python3 -m pip install Pillow numpy scipy gTTS pyttsx3

sudo git clone https://github.com/BoseCorp/py-googletrans.git #this repository corrects an error contained in the official 

cd ./py-googletrans 

sudo python3 setup.py install 
```

## Running the Code


There are two versions of the code, one offline that makes use of pyttsx3 and another online using gTTS. 
To copy the repository: 

```sh
git clone https://github.com/QualcommIoTReferenceCenter/Text2Speech.git
cd Text2Speech
```

Before starting the application, if you are going to use the audio output feature, set a bluetooth connection with the device
that will be used, follow next steps:

```sh
bash docs/bluetooth.sh 
```

If you want to connect manually from the Bluetooth on the Dragonboard, follow the following step by step:

```sh
sudo bluetoothctl

scan on # Make a note of the [MAC ADDRESS] of the device you want to connect

trust [MAC ADDRESS]

pair [MAC ADDRESS]

connect [MAC ADDRESS]
```

Open the application by running the command:

```sh
python3 App.py 
```

**Possible error during execution**

There may be some error because the program can not create the .png file for Tesseract to do the acknowledgment. If this happens, change the folder permissions as follows: 

```sh
cd ..

sudo chmod 777 Text2Speech 

cd Text2Speech 

sudo chmod 777 libs

cd Text2Speech 
```

Another error that can occur is regarding the definition of the display you are using, if this problem occurs it can be solved through the following command:

```sh
DISPLAY=:0.0
```

## Examples

*Example 1* - App Functionality

On the interface screen there are two options for operating the code: online and offline. The offline mode performs the reading in Portuguese and English, according to the chosen option, and the text is played in audio format.
In the online mode, the Google API (gTTS) is used, which performs the translation and reproduction of the text and the languages ​​can be chosen.

 <div align="center">
    <figure>
        <img src="/Tutorial/App_read_trans.png">
        <figcaption>App Screen</figcaption>
    </figure>
</div>

To perform the reading, point the webcam to the text, choose one of the options on the screen and press the play button. After processing, the text that is in the video will be written and played by the interface.

 <div align="center">
    <figure>
        <img src="/Tutorial/a_02.png">
        <figcaption>Reading mode</figcaption>
    </figure>
</div>

To do the translation, choose the language options and press the translate button. After processing, the API will translate the text, the text will be displayed and played back in the interface audio.

 <div align="center">
    <figure>
        <img src="/Tutorial/App_trans.png">
        <figcaption>Translation mode</figcaption>
    </figure>
</div>

*Example 2* - The following are some screenshots of how the application detects text:

 <div align="center">
    <figure>
        <img src="/Tutorial/ok_01.PNG">
        <figcaption>Simple word detection (paper)</figcaption>
    </figure>
</div>

 <div align="center">
    <figure>
        <img src="/Tutorial/ok_02.PNG">
        <figcaption>Full phrase detection (paper)</figcaption>
    </figure>
</div>

 <div align="center">
    <figure>
        <img src="/Tutorial/ok_03.PNG">
        <figcaption>Full Text Detection (e-reader)</figcaption>
    </figure>
</div>

 <div align="center">
    <figure>
        <img src="/Tutorial/App_read_text.png">
        <figcaption>Full Text Detection with special characters (paper)</figcaption>
    </figure>
</div>

After reading or translating, you must clear the settings with a double-click anywhere in the interface, so that you can display a new text. 

## Limitations

 Text detection is highly dependent on the quality of the photographed image. For better operation of the application, it is recommended to use a mobile USB camera to facilitate movement and framing, take care to tilt the text to be readable, and avoid reflections that compromise the surface to be photographed (on paper the reflections do not show much problem, but performing tests with text on cell phones, the factor influences the result a lot).


Lastly, because the text translation feature uses the Google translator API, minor errors can occur, especially in isolated context phrases and words.
