import pyttsx3

# Pyttsx3 is a offline "speecher"
def speech_offline(text_string, mode):
    # pyttsx3's contructor
    engine = pyttsx3.init()

    """ RATE """
    engine.setProperty('rate', 120)
    
    """ VOLUME """
    engine.setProperty('volume', 1.0)

    """ VOICE """
    voices = engine.getProperty('voices')
    for voice in voices:
        if mode == True and voice.id == 'brazil':
            engine.setProperty('voice', voice.id)
        elif mode == False and voice.id == 'english-us':
            engine.setProperty('voice', voice.id)

    """ SPEECH """
    engine.say(text_string)
    engine.runAndWait()
    engine.stop()

    return
