""" 
    Pyttsx3 has many speech languages, then It's possible
    to define witch language you want to use.

    This algorithms show all speech language options and set a specifc 
    language, in this case 'english-us'.
    
    """
    
import pyttsx3

engine = pyttsx3.init()

""" RATE """
engine.setProperty('rate', 125)

""" VOLUME """
engine.setProperty('volume', 1.0)

""" VOICE """
# Show a list of speech languages
voices = engine.getProperty('voices')
for voice in voices:
    print("Voice: %s" % voice.name)
    print(" - ID: %s" % voice.id)
    print(" - Languages: %s" % voice.languages)
    print(" - Gender: %s" % voice.gender)
    print(" - Age: %s" % voice.age)
    print("\n")

# Set the speech language in english 
voices = engine.getProperty('voices')
for voice in voices:
    if voice.id == 'english-us':
        engine.setProperty('voice', voice.id)

""" SPEECH """
engine.say("Hello World")
engine.runAndWait()
engine.stop()