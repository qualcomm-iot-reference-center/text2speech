
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importing libraries
import PIL.Image, PIL.ImageTk, PIL.ImageOps
import shlex, subprocess
import tkinter
import cv2

from tkinter import Frame, Label, messagebox
from tkinter.ttk import Combobox
from libs.process_image import*
from libs.text_recog import*
from libs.speech_online import*
from libs.speech_offline import*
from libs.trans_text import*

# Function to execute a command in bash
def python_to_bash(comando_bash):
    args = shlex.split(comando_bash)
    comando = subprocess.Popen(args, stdout=subprocess.PIPE, shell=True)
    saida, erro = comando.communicate()
    comando = subprocess.check_output(args)
    return comando

# Application Class Object
class App:
    def __init__(self, window, window_title, video):
	    #window       
        self.window = window
        self.video = video
	
        window.title(window_title)
        window.configure(background='#B5DEFA', bd=40, relief= "groove")
        window_width, window_height = 800,1000 
        window.geometry('{}x{}'.format(window_width, window_height))
        
        # Options for DropDown list
        OPTIONS = {
            'af': 'afrikaans',
            'sq': 'albanian',
            'am': 'amharic',
            'ar': 'arabic',
            'hy': 'armenian',
            'az': 'azerbaijani',
            'eu': 'basque',
            'be': 'belarusian',
            'bn': 'bengali',
            'bs': 'bosnian',
            'bg': 'bulgarian',
            'ca': 'catalan',
            'ceb': 'cebuano',
            'ny': 'chichewa',
            'zh-cn': 'chinese (simplified)',
            'zh-tw': 'chinese (traditional)',
            'co': 'corsican',
            'hr': 'croatian',
            'cs': 'czech',
            'da': 'danish',
            'nl': 'dutch',
            'en': 'english',
            'eo': 'esperanto',
            'et': 'estonian',
            'tl': 'filipino',
            'fi': 'finnish',
            'fr': 'french',
            'fy': 'frisian',
            'gl': 'galician',
            'ka': 'georgian',
            'de': 'german',
            'el': 'greek',
            'gu': 'gujarati',
            'ht': 'haitian creole',
            'ha': 'hausa',
            'haw': 'hawaiian',
            'iw': 'hebrew',
            'hi': 'hindi',
            'hmn': 'hmong',
            'hu': 'hungarian',
            'is': 'icelandic',
            'ig': 'igbo',
            'id': 'indonesian',
            'ga': 'irish',
            'it': 'italian',
            'ja': 'japanese',
            'jw': 'javanese',
            'kn': 'kannada',
            'kk': 'kazakh',
            'km': 'khmer',
            'ko': 'korean',
            'ku': 'kurdish (kurmanji)',
            'ky': 'kyrgyz',
            'lo': 'lao',
            'la': 'latin',
            'lv': 'latvian',
            'lt': 'lithuanian',
            'lb': 'luxembourgish',
            'mk': 'macedonian',
            'mg': 'malagasy',
            'ms': 'malay',
            'ml': 'malayalam',
            'mt': 'maltese',
            'mi': 'maori',
            'mr': 'marathi',
            'mn': 'mongolian',
            'my': 'myanmar (burmese)',
            'ne': 'nepali',
            'no': 'norwegian',
            'ps': 'pashto',
            'fa': 'persian',
            'pl': 'polish',
            'pt': 'portuguese',
            'pa': 'punjabi',
            'ro': 'romanian',
            'ru': 'russian',
            'sm': 'samoan',
            'gd': 'scots gaelic',
            'sr': 'serbian',
            'st': 'sesotho',
            'sn': 'shona',
            'sd': 'sindhi',
            'si': 'sinhala',
            'sk': 'slovak',
            'sl': 'slovenian',
            'so': 'somali',
            'es': 'spanish',
            'su': 'sundanese',
            'sw': 'swahili',
            'sv': 'swedish',
            'tg': 'tajik',
            'ta': 'tamil',
            'te': 'telugu',
            'th': 'thai',
            'tr': 'turkish',
            'uk': 'ukrainian',
            'ur': 'urdu',
            'uz': 'uzbek',
            'vi': 'vietnamese',
            'cy': 'welsh',
            'xh': 'xhosa',
            'yi': 'yiddish',
            'yo': 'yoruba',
            'zu': 'zulu',
            'fil': 'Filipino',
            'he': 'Hebrew'
        }
        # Inverting the OPTIONS dict to work with
        self.LANGCODES = dict(map(reversed, OPTIONS.items()))

        #title
        self.frame3 = Frame(window)
        self.frame3.pack(side=tkinter.TOP)

        #label title
        self.lbl1= tkinter.Label(self.frame3, text="Tecnologia Assistiva", width=700,height=2, font=("Arial", 32, "bold"), fg="#447CC2", bg="#B5DEFA").pack()
        self.image = PIL.Image.open('text2speech.png')
        self.image = self.image.resize((60, 60), PIL.Image.ANTIALIAS)
        self.image = PIL.ImageTk.PhotoImage(self.image)
        self.lbl2= tkinter.Label(self.frame3, image=self.image, bg="#B5DEFA").place(x=50, y=8)
        
        #label menu
        self.frame3_menu = Frame(window,bg="#B5DEFA")
        self.frame3_menu.pack(side=tkinter.TOP, anchor= tkinter.CENTER)

        self.frame3_trans = tkinter.Button(self.frame3_menu, text="Tradução", font=("Arial", 10, "bold"), fg="#447CC2", bg="#B5DEFA", command=self.translation).grid(row=1,column=1)
        self.frame3_read = tkinter.Button(self.frame3_menu, text="Leitura", font=("Arial", 10, "bold"), fg="#447CC2", bg="#B5DEFA", command=self.read).grid(row=1,column=3)
        
        self.frame4_trans = tkinter.Label(self.frame3_menu, text="Traduzir de:", font=("Arial", 10, "bold"), height=2,fg="#447CC2", bg="#B5DEFA").grid(row=2,column=1)
        self.frame4_read = tkinter.Label(self.frame3_menu, text="Para:", font=("Arial", 10, "bold"), height=2,fg="#447CC2", bg="#B5DEFA").grid(row=3,column=1)

        # DropDown list
        self.language = tkinter.StringVar(window)
        self.language.set(OPTIONS['pt']) # default value

        DropDown_list = [i for i in self.LANGCODES.keys()]
        w = Combobox(self.frame3_menu, textvariable = self.language, values = DropDown_list)
        w.grid(row=2,column=2)
        
        self.language2 = tkinter.StringVar(window)
        self.language2.set(OPTIONS['pt']) # default value

        w2 = Combobox(self.frame3_menu, textvariable = self.language2, values = DropDown_list)
        w2.grid(row=3,column=2)
        
        # Read check boxes (portuguese or english)
        self.port = tkinter.IntVar()
        self.frame5_port = tkinter.Checkbutton(self.frame3_menu, text="Português", variable=self.port, onvalue = 1, offvalue = 0, font=("Arial", 10, "bold"), fg="#447CC2", bg="#B5DEFA").grid(row=2,column=3,padx=20)
        self.eng = tkinter.IntVar()
        self.frame5_eng = tkinter.Checkbutton(self.frame3_menu, text="Inglês", variable=self.eng, onvalue = 1, offvalue = 0, font=("Arial", 10, "bold"), fg="#447CC2", bg="#B5DEFA").grid(row=3,column=3,padx=20)
        
	    #video
        self.frame1 = Frame(window, bd=2, bg="#447CC2")
        self.frame1.pack(anchor=tkinter.CENTER)

        #button 
        self.btn1= tkinter.Button(window, text="Sair", font=("Arial", 10, "bold"), width =5, height =2, bg="#B5DEFA")
        self.btn1.bind("<Button-1>",self.stop)
        self.btn1.pack(side=tkinter.BOTTOM)

       
        self.frame4 = Frame(window, bg="#B5DEFA")
        self.frame4.pack(side=tkinter.BOTTOM)

        #window.bind("<h>", self.languages)
        #window.bind("<Return>",self.clean)

        self.vid = MyVideoCapture(self.video)
        self.canvas = tkinter.Canvas(self.frame1, width = self.vid.width, height = self.vid.height, bg="white")
        self.canvas.pack()
        self.delay = 15
        self.update()
        self.window.mainloop()
        

    def update(self):
        # Get a frame
        ret,frame = self.vid.get_frame()
        if ret:
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)

        self.window.after(self.delay, self.update)

    # Read option, portuguese or english (offline)
    def read(self):
        port = self.port.get()
        eng = self.eng.get()
        if port == 0 and eng == 0:
            messagebox.showinfo("Leitura","Por favor, selecione uma das opções")
        elif port == 1:
            lang = 'por'
            mode = True
        elif eng == 1:
            lang = 'eng'
            mode = False

        if port == 1 or eng == 1:
            ret,frame = self.vid.get_frame()
            if ret:
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                picture = process_image(gray)
                
                text = text_recog(picture, lang)
                
                self.result = tkinter.Label(self.frame4, text=text, width=50,height=20,font=("Arial", 8, "bold"), fg="black", bg="#B5DEFA")
                self.result.pack(side=tkinter.BOTTOM)
                speech = speech_offline(text, mode)
                self.result.destroy()

    # Translation option, many languages (online)
    def translation(self):
        # Languages that pytesseract can recognize
        REGOG_OPTIONS = {
            'af': 'afr',
            'sq': 'sqi',
            'am': 'amh',
            'ar': 'ara',
            'az': 'aze_cyrl',
            'eu': 'eus',
            'be': 'bel',
            'bn': 'ben',
            'bs': 'bos',
            'bg': 'bul',
            'ca': 'cat',
            'ceb': 'ceb',
            'ny': 'chichewa',
            'zh-cn': 'chi_sim',
            'zh-tw': 'chi_tra',
            'hr': 'hrv',
            'cs': 'ces',
            'da': 'dan',
            'nl': 'nld',
            'en': 'eng',
            'eo': 'epo',
            'et': 'est',
            'tl': 'tgl',
            'fi': 'fin',
            'fr': 'fra',
            'gl': 'glg',
            'ka': 'kat',
            'de': 'deu',
            'el': 'ell',
            'gu': 'guj',
            'ht': 'hat',
            'iw': 'heb',
            'hi': 'hin',
            'hu': 'hun',
            'is': 'isl',
            'id': 'ind',
            'ga': 'gle',
            'it': 'ita',
            'ja': 'jpn',
            'jw': 'jav',
            'kn': 'kan',
            'kk': 'kaz',
            'km': 'khm',
            'ko': 'kor',
            'ku': 'kur',
            'ky': 'kir',
            'lo': 'lao',
            'la': 'lat',
            'lv': 'lav',
            'lt': 'lit',
            'mk': 'mkd',
            'ms': 'msa',
            'ml': 'mal',
            'mt': 'mlt',
            'mr': 'mar',
            'my': 'mya',
            'ne': 'nep',
            'no': 'nor',
            'ps': 'pus',
            'fa': 'fas',
            'pl': 'pol',
            'pt': 'por',
            'pa': 'pan',
            'ro': 'ron',
            'ru': 'rus',
            'sr': 'srp',
            'si': 'sin',
            'sk': 'slk',
            'sl': 'slv',
            'es': 'spa',
            'sw': 'swa',
            'sv': 'swe',
            'tg': 'tgk',
            'ta': 'tam',
            'te': 'tel',
            'th': 'tha',
            'tr': 'tur',
            'uk': 'ukr',
            'ur': 'urd',
            'uz': 'uzb',
            'vi': 'vie',
            'cy': 'cym',
            'yi': 'yid',
            'fil': 'tgl',
            'he': 'heb'
        }

        # The language of the source text
        self.src=self.LANGCODES[self.language.get()]
        # Check if pytesseract can recognize the language selected
        try:
            if self.src in REGOG_OPTIONS.keys():
                lang = REGOG_OPTIONS[self.src]
        except:
            print("***WARNING*** - The language that you have selected may not be possible\
            to recognize")
            lang = 'eng'

        # The language to translate the source text into
        self.dest=self.LANGCODES[self.language2.get()]

        ret,frame = self.vid.get_frame()
        if ret:
            
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            picture = process_image(gray)
            
            text = text_recog(picture, lang)

            translate_text = trans_text(text,self.dest,self.src)
            
            print (translate_text)
            
            self.result = tkinter.Label(self.frame4, text=translate_text, width=50,height=20,font=("Arial", 8, "bold"), fg="black", bg="#B5DEFA")
            self.result.pack(side=tkinter.BOTTOM)
            speech = speech_online(translate_text, self.dest)
            self.result.destroy()

    def clean(self):

        self.language.set("")
        self.language2.set("")
        self.result.destroy()
        
    def stop(self,event):
        
        self.window.destroy()

    def languages(self,event):
        self.languages = tkinter.Toplevel(self.window)
        self.options = Window(self.languages)

class MyVideoCapture:
     def __init__(self, video):
         # Open the video source
         self.vid = cv2.VideoCapture(video)
         if not self.vid.isOpened():
             raise ValueError("Unable to open video source", video)
         self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
         self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
 
     def get_frame(self):
         if self.vid.isOpened():
             ret, frame = self.vid.read()
             if ret:
                 # Return a boolean success flag and the current frame converted to BGR
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
             else:
                 return (ret, None)
         else:
             return (ret, None)
 
     # Release the video source when the object is destroyed
     def __del__(self):
         if self.vid.isOpened():
             self.vid.release()
        

if __name__=='__main__':
    
    lista_dispositivos = 'v4l2-ctl --list-devices'
    saida = python_to_bash(lista_dispositivos)
    saida = saida.decode(encoding='UTF-8')
    
    lista_saida = saida.split('\n')
    
    for n_linha, texto in enumerate(lista_saida):
        if 'usb' in texto:
            nome_camera = lista_saida[n_linha + 1].strip()
            print(nome_camera[-1])
    
    App(tkinter.Tk(),"Tecnologia assistiva",video=int(nome_camera[-1]))

    
